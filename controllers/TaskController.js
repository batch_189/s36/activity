const Task = require('../models/Task.js');


module.exports.createTask = (requestBody) => {

    let newTask = new Task({
        name: requestBody.name //name from postman input
    });
    
    return newTask.save().then((savedTasks, error) => {
        if (error){
            return error
        }
            return 'Task created successfully'

    });
};

module.exports.getTask = (taskId) => {
   return Task.findById(taskId).then((retrievedOneTask, error) => {
        if (error){
            return error
        } else {
            return retrievedOneTask
        }
    });
};

module.exports.updateTask = (taskId, newValue) => {

    return Task.findById(taskId).then((result, error) => {
        if (error){
            return error
        } 
        result.status = newValue.status

        return result.save().then((updatedTask, error) => {
            if (error){
                return error
            } else {
                return updatedTask
            }
        });
    });
};