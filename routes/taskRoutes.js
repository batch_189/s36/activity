const { request, response } = require('express');
const express = require('express');
const TaskController = require('../controllers/TaskController.js');
const task = require('../models/Task.js');
const router = express.Router();

router.post('/create', (request, response) => {
    TaskController.createTask(request.body).then((task) => response.send(task))
});

router.get('/:id', (request, response) => {
    TaskController.getTask(request.params.id).then((oneTask) => response.send(oneTask))
});

router.put('/:id/update', (request, response) => {
    TaskController.updateTask(request.params.id, request.body).then((updateTask) => response.send(updateTask));
})
module.exports = router